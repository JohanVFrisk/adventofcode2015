mod day01;
mod day02;
mod day03;
mod day04;
mod day05;
mod day06;

use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        println!("Select a problem");
        return;
    }

    let problem = args[1].parse::<u8>();

    match problem {
        Ok(1) => day01::main(),
        Ok(2) => day02::main(),
        Ok(3) => day03::main(),
        Ok(4) => day04::main(),
        Ok(5) => day05::main(),
        Ok(6) => day06::main(),
        Ok(_) => println!("Unknown problem"),
        Err(_) => println!("Select a problem")
    };
}
