use super::*;

#[test]
fn one() {
    assert_eq!(part1("".into()), 0);
}

#[test]
fn two() {
    assert_eq!(part2("".into()), 0);
}
