fn part1(input: String) -> usize {
    unimplemented!()
}

fn part2(input: String) -> usize {
    unimplemented!()
}

pub fn main() {
    println!("Part 1 answer is: {}", part1(parse()));
    println!("Part 2 answer is: {}", part2(parse()));
}

fn parse() -> String {
    std::fs::read_to_string("src/day00/input.txt").expect("Unable to read input")
}

#[cfg(test)]
mod tests;
