use std::fs;

fn part1(input: String) -> i64 {
    // To what floor do the instructions take Santa?
    input.chars().fold(0, |acc,c| acc + match c {
        '(' => 1,
        ')' => -1,
        _ => 0,
    })
}

fn part2(input: String) -> i64 {
    // What is the position of the character that causes Santa to first enter the basement?
    let mut floor = 0;
    let mut pos = 0;
    let mut chars = input.chars();
    while let Some(c) = chars.next() {
        pos += 1;
        floor += match c {
            '(' => 1,
            ')' => -1,
            _ => 0,
        };
        if floor <= -1 {
            return pos;
        }
    }
    -1
}

pub fn main() {
    println!("Part 1 answer is: {}", part1(parse()));
    println!("Part 2 answer is: {}", part2(parse()));
}

fn parse() -> String {
    fs::read_to_string("src/day01/input.txt").expect("Unable to read input")
}

#[cfg(test)]
mod tests;
