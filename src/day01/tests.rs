use super::*;

#[test]
fn one() {
    assert_eq!(part1("(())".into()), 0);
    assert_eq!(part1("()()".into()), 0);
    assert_eq!(part1("(((".into()), 3);
    assert_eq!(part1("(()(()(".into()), 3);
    assert_eq!(part1("))(((((".into()), 3);
    assert_eq!(part1("())".into()), -1);
    assert_eq!(part1("))(".into()), -1);
    assert_eq!(part1(")))".into()), -3);
    assert_eq!(part1(")())())".into()), -3);
}

#[test]
fn two() {
    assert_eq!(part2(")".into()), 1);
    assert_eq!(part2("()())".into()), 5);
    assert_eq!(part2("()()))()".into()), 5);
}
