fn part1(input: String) -> usize {
    input.lines().fold(0, |acc, string|
        acc + match is_nice(string) {
            true => 1,
            false => 0,
        }
    )
}

fn is_nice(s: &str) -> bool {
    let bad = vec!["ab", "cd", "pq", "xy"];
    for word in bad {
        if s.contains(&word) {
            return false;
        }
    }
    
    let count_vowels = s.chars().fold(0, |acc, c| acc + if "aeiou".contains(c) {1} else {0});
    if count_vowels < 3 {
        return false;
    }

    let mut chars = s.chars();
    let mut last = chars.next();
    let mut char = chars.next();
    while let Some(c) = char {
        if c == last.unwrap() {
            return true;
        } else {
            last = char;
            char = chars.next();
        }
    }

    false
}

fn part2(input: String) -> usize {
    input.lines().fold(0, |acc, string|
        acc + match is_nice_2(string) {
            true => 1,
            false => 0,
        }
    )
}

fn is_nice_2(s: &str) -> bool {
    if !has_non_overlapping_pair(s) {
        return false;
    }

    if !has_repeating_letter_with_one_between(s) {
        return false;
    }

    true
}

fn has_non_overlapping_pair(s: &str) -> bool {
    let chars: Vec<char> = s.chars().collect();

    for i in 0..chars.len() - 3 {
        let pair = &s[i..i + 2];
        if s[i + 2..].contains(pair) {
            return true;
        }
    }

    false
}

fn has_repeating_letter_with_one_between(s: &str) -> bool {
    let chars: Vec<char> = s.chars().collect();

    for i in 0..chars.len() - 2 {
        if chars[i] == chars[i + 2] {
            return true;
        }
    }

    false
}

pub fn main() {
    println!("Part 1 answer is: {}", part1(parse()));
    println!("Part 2 answer is: {}", part2(parse()));
}

fn parse() -> String {
    std::fs::read_to_string("src/day05/input.txt").expect("Unable to read input")
}

#[cfg(test)]
mod tests;
