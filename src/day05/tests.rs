use super::*;

#[test]
fn one() {
    assert_eq!(part1("ugknbfddgicrmopn".into()), 1);
    assert_eq!(part1("aaa".into()), 1);
    assert_eq!(part1("jchzalrnumimnmhp".into()), 0);
    assert_eq!(part1("haegwjzuvuyypxyu".into()), 0);
    assert_eq!(part1("dvszwmarrgswjxmb".into()), 0);
}

#[test]
fn two() {
    assert_eq!(part2("qjhvhtzxzqqjkmpb".into()), 1);
    assert_eq!(part2("xxyxx".into()), 1);
    assert_eq!(part2("uurcxstgmygtbstg".into()), 0);
    assert_eq!(part2("ieodomkazucvgmuy".into()), 0);
}
