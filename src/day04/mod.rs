use md5;


fn part1(input: String) -> usize {
    let mut stuff = 0;
    while !hash(format!("{input}{stuff}")).starts_with("00000")  {
        stuff += 1;
    }
    stuff
}

fn part2(input: String) -> usize {
    let mut stuff = 0;
    while !hash(format!("{input}{stuff}")).starts_with("000000")  {
        stuff += 1;
    }
    stuff
}

fn hash(s: String) -> String {
    format!("{:x}", md5::compute(s.as_bytes()))
}

pub fn main() {
    println!("Part 1 answer is: {}", part1("yzbqklnj".into()));
    println!("Part 2 answer is: {}", part2("yzbqklnj".into()));
}

#[cfg(test)]
mod tests;
