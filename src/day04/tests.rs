use super::*;


#[test]
fn md5_test() {
    assert!(hash("abcdef609043".to_string()).starts_with("000001dbbfa"));
    assert!(hash("pqrstuv1048970".to_string()).starts_with("000006136ef"));
}


#[test]
fn one() {
    assert_eq!(part1("abcdef".into()), 609043);
    assert_eq!(part1("pqrstuv".into()), 1048970);
}

