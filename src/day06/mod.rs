use regex::Regex;

type LightGrid = [[bool; 1000]; 1000];
type LightGrid2 = [[usize; 1000]; 1000];

enum Operation {
    Toggle,
    TurnOn,
    TurnOff,
    Unknown,
}

fn set_lights(lights: &mut LightGrid, op: Operation, x_start: usize, y_start: usize, x_end: usize, y_end: usize) {
    for x in x_start..=x_end {
        for y in y_start..=y_end {
            lights[x][y] = match op {
                Operation::Toggle => !lights[x][y],
                Operation::TurnOn => true,
                Operation::TurnOff => false,
                Operation::Unknown => lights[x][y],
            };
        }
    }
}

fn calculate_lights(lights: &LightGrid) -> usize {
    let mut count = 0;

    for row in lights.iter() {
        for &cell in row.iter() {
            if cell {
                count += 1;
            }
        }
    }

    count
}

fn get_numbers(line: &str) -> Vec<usize> {
    let re = Regex::new(r"(\d+)").unwrap();
    let mut numbers = Vec::new();

    for capture in re.captures_iter(line) {
        let number_str = &capture[1];
        let number = number_str.parse::<usize>().expect("Failed to parse usize");
        numbers.push(number);
    }

    numbers
}

fn get_operation(line: &str) -> Operation {

    let mut words = line.split_whitespace();
    match (words.next(), words.next()) {
        (Some("turn"), Some("on")) => Operation::TurnOn,
        (Some("turn"), Some("off")) => Operation::TurnOff,
        (Some("toggle"), _) => Operation::Toggle,
        _ => Operation::Unknown,
    }
}

fn part1(input: String) -> usize {

    let mut lights: LightGrid = [[false; 1000]; 1000];

    for line in input.lines() {
        let numbers = get_numbers(line);
        let operation = get_operation(line);

        set_lights(&mut lights, operation, numbers[0], numbers[1], numbers[2], numbers[3]);
    }
    
    calculate_lights(&lights)
}

fn set_lights2(lights: &mut LightGrid2, op: Operation, x_start: usize, y_start: usize, x_end: usize, y_end: usize) {
    for x in x_start..=x_end {
        for y in y_start..=y_end {
            lights[x][y] = match op {
                Operation::Toggle => lights[x][y] + 2,
                Operation::TurnOn => lights[x][y] + 1,
                Operation::TurnOff => std::cmp::max(lights[x][y], 1) - 1,
                Operation::Unknown => lights[x][y],
            };
        }
    }
}

fn calculate_lights2(lights: &LightGrid2) -> usize {
    let mut count = 0;

    for row in lights.iter() {
        for &cell in row.iter() {
            count += cell;
        }
    }

    count
}

fn part2(input: String) -> usize {
    let mut lights: LightGrid2 = [[0; 1000]; 1000];

    for line in input.lines() {
        let numbers = get_numbers(line);
        let operation = get_operation(line);

        set_lights2(&mut lights, operation, numbers[0], numbers[1], numbers[2], numbers[3]);
    }
    
    calculate_lights2(&lights)

}

pub fn main() {
    println!("Part 1 answer is: {}", part1(parse()));
    println!("Part 2 answer is: {}", part2(parse()));
}

fn parse() -> String {
    std::fs::read_to_string("src/day06/input.txt").expect("Unable to read input")
}

#[cfg(test)]
mod tests;
