use super::*;

#[test]
fn one() {
    assert_eq!(part1("turn on 0,0 through 999,999\n".into()), 1000*1000);
    assert_eq!(part1("toggle 0,0 through 999,0\n".into()), 1000);
    assert_eq!(part1("turn on 0,0 through 999,999\nturn off 499,499 through 500,500\n".into()), 1000*1000 - 4);
}

#[test]
fn two() {
    // TODO: stack overflow
    todo!();
    //assert_eq!(part2("turn on 0,0 through 999,0\n".into()), 1000);
    //assert_eq!(part2("turn on 0,0 through 999,999\nturn on 0,0 through 999,999\n".into()), 2*1000*1000);
    //assert_eq!(part2("toggle 0,0 through 999,999\nturn on 0,0 through 999,999\n".into()), 3*1000*1000);
}
