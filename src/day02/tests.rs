use super::*;

#[test]
fn one() {
    assert_eq!(part1("2x3x4".into()), 58);
    assert_eq!(part1("1x1x10".into()), 43);
    assert_eq!(part1("2x3x4\n1x1x10".into()), 58+43);
}

#[test]
fn two() {
    assert_eq!(part2("2x3x4".into()), 34);
    assert_eq!(part2("1x1x10".into()), 14);
    assert_eq!(part2("2x3x4\n1x1x10".into()), 34+14);
}
