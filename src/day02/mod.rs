use std::fs;
use std::cmp::min;

fn part1(input: String) -> i64 {
    // How many total square feet of wrapping paper should they order?
    input.lines().fold(0, |acc, present| acc + {
            let p: Vec<&str> = present.split('x').collect();
            let l = p[0].parse::<i64>().unwrap();
            let w = p[1].parse::<i64>().unwrap();
            let h = p[2].parse::<i64>().unwrap();
            2*l*w + 2*w*h + 2*h*l + min(min(l*w, w*h), h*l)
        }
    )
}

fn part2(input: String) -> i64 {
    // How many total feet of ribbon should they order?
    input.lines().fold(0, |acc, present| acc + {
            let p: Vec<&str> = present.split('x').collect();
            let l = p[0].parse::<i64>().unwrap();
            let w = p[1].parse::<i64>().unwrap();
            let h = p[2].parse::<i64>().unwrap();
            w * h * l + min(min(l+l+w+w, w+w+h+h), h+h+l+l)
        }
    )
}

pub fn main() {
    println!("Part 1 answer is: {}", part1(parse()));
    println!("Part 2 answer is: {}", part2(parse()));
}

fn parse() -> String {
    fs::read_to_string("src/day02/input.txt").expect("Unable to read input")
}

#[cfg(test)]
mod tests;
