use super::*;

#[test]
fn one() {
    assert_eq!(part1(">".into()), 2);
    assert_eq!(part1("^>v<".into()), 4);
    assert_eq!(part1("^v^v^v^v^v".into()), 2);
}

#[test]
fn two() {
    assert_eq!(part2("^v".into()), 3);
    assert_eq!(part2("^>v<".into()), 3);
    assert_eq!(part2("^v^v^v^v^v".into()), 11);
}
